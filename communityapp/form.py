from django import forms
from django.contrib.auth.models import User
from .models import profile, SubProfile, ImageBlog, SubImageBlog


class UserCreateForm(forms.ModelForm):
    number = forms.IntegerField(label="phone number / ફોન નંબર")
    password = forms.CharField(widget=forms.PasswordInput,label="password / પાસવર્ડ")
    password2 = forms.CharField(widget=forms.PasswordInput,label="confirm password / પાસવર્ડની પુષ્ટિ કરો")

    class Meta:
        model = User
        fields = ('number', 'password', 'password2')


class UserLoginForm(forms.ModelForm):
    number = forms.IntegerField(label="phone number / ફોન નંબર")
    password = forms.CharField(widget=forms.PasswordInput,label="password / પાસવર્ડ")

    class Meta:
        model = User
        fields = ('number', 'password')


class UserProfileCreateForm(forms.ModelForm):
    class Meta:
        model = profile
        fields = ['first_name', 'last_name', 'father_name', 'birth_date', 'genre', 'address', 'area', 'city',
                  'state', 'country', 'pincode', 'phone', 'email', 'marital_status', 'education',
                  'education_detail', 'occupation', 'occupation_detail']


class SubProfileCreateForm(forms.ModelForm):
    class Meta:
        model = SubProfile
        fields = ['relation','first_name', 'last_name', 'father_name', 'birth_date', 'genre', 'address', 'area', 'city',
                  'state', 'country', 'pincode', 'phone', 'email', 'marital_status', 'education',
                  'education_detail', 'occupation', 'occupation_detail']


class ImageBlogCreateForm(forms.ModelForm):
    class Meta:
        model = ImageBlog
        fields = ['imageblog_title', 'mainimage']


class SubImageBlogCreateForm(forms.ModelForm):
    class Meta:
        model = SubImageBlog
        fields = ['extraimage']
