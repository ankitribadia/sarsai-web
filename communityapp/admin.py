from django.contrib import admin
from .models import profile, blog, SubProfile, ImageBlog, SubImageBlog


class SubProfileAdmin(admin.ModelAdmin):
    list_display = ["id", "first_name", "last_name", "father_name", "updated", "created"]
    list_filter = ["updated", "created"]
    search_fields = ["id", "first_name", "last_name", "father_name"]
    list_display_links = ['id']
    list_max_show_all = 100
    list_per_page = 25

    class Meta:
        model = SubProfile


class ProfileAdmin(admin.ModelAdmin):
    list_display = ["id", "first_name", "last_name", "father_name", "updated", "created"]
    list_filter = ["updated", "created"]
    search_fields = ["id", "first_name", "last_name", "father_name"]
    list_display_links = ['id']
    list_max_show_all = 100
    list_per_page = 25

    class Meta:
        model = profile


class BlogAdmin(admin.ModelAdmin):
    list_display = ["id", "blog_title", "blog_text"]
    search_fields = ["id", "blog_title", "blog_text"]
    list_display_links = ['id']
    list_max_show_all = 100
    list_per_page = 25

    class Meta:
        model = blog

class ImageBlogAdmin(admin.ModelAdmin):
    list_display = ["id", "imageblog_title", "mainimage"]
    search_fields = ["id", "imageblog_title", "mainimage"]
    list_display_links = ['id']
    list_max_show_all = 100
    list_per_page = 25

    class Meta:
        model = blog

class SubImageBlogAdmin(admin.ModelAdmin):
    list_display = ["id", "extraimage"]
    search_fields = ["id", "extraimage"]
    list_display_links = ['id']
    list_max_show_all = 100
    list_per_page = 25

    class Meta:
        model = blog

admin.site.register(SubProfile, SubProfileAdmin)
admin.site.register(profile, ProfileAdmin)
admin.site.register(blog, BlogAdmin)
admin.site.register(ImageBlog,ImageBlogAdmin)
admin.site.register(SubImageBlog,SubImageBlogAdmin)