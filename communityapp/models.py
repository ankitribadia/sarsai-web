from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save
from django.urls import reverse


class ProfileManager(models.Manager):
    def ExcludeAdminUser(self):
        qs = super(ProfileManager, self).all().exclude(user=1)
        return qs


class profile(models.Model):
    GENRE_CHOICES = (
        ('m', 'Male'),
        ('f', 'Female'),
    )
    MARITAL_STATUS_CHOICES = (
        ('s', 'Single'),
        ('m', 'Marride'),
        ('d', 'Divorced'),
        ('w', 'Widowed'),
    )

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    father_name = models.CharField(max_length=50, null=True, blank=True)
    birth_date = models.CharField(max_length=20, null=True, blank=True)
    genre = models.CharField(max_length=1, choices=GENRE_CHOICES, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    area = models.CharField(max_length=150, null=True, blank=True)
    city = models.CharField(max_length=15, null=True, blank=True)
    state = models.CharField(max_length=15, null=True, blank=True)
    country = models.CharField(max_length=15, null=True, blank=True)
    pincode = models.CharField(max_length=6, null=True, blank=True)
    phone = models.CharField(max_length=10, null=True, blank=True)
    email = models.CharField(max_length=150, null=True, blank=True)
    marital_status = models.CharField(max_length=1, choices=MARITAL_STATUS_CHOICES, null=True, blank=True)
    education = models.CharField(max_length=150, null=True, blank=True)
    education_detail = models.TextField(null=True, blank=True)
    occupation = models.CharField(max_length=150, null=True, blank=True)
    occupation_detail = models.TextField(null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False, auto_created=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True, auto_created=True)

    objects = ProfileManager()

    def __str__(self):
        return str(self.phone)

    def get_absolute_url(self):
        return reverse("profile:profile_detail", kwargs={"id": self.id})

    def get_update_url(self):
        return reverse("profile:profile_update", kwargs={"id": self.id})

    class Meta:
        ordering = ["id"]


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile.objects.create(user=instance, phone=instance.username)
    instance.profile.save()


post_save.connect(create_user_profile, sender=User)


class SubprofileManager(models.Manager):
    def filter_by_instance(self, instance):
        content_type = ContentType.objects.get_for_model(instance.__class__)
        obj_id = instance.id
        qs = super(SubprofileManager, self).filter(content_type=content_type, object_id=obj_id)
        return qs


class SubProfile(models.Model):
    GENRE_CHOICES = (
        ('m', 'Male'),
        ('f', 'Female'),
    )
    MARITAL_STATUS_CHOICES = (
        ('s', 'Single'),
        ('m', 'Marride'),
        ('d', 'Divorced'),
        ('w', 'Widowed'),
    )

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    relation = models.CharField(max_length=10, null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    father_name = models.CharField(max_length=50, null=True, blank=True)
    birth_date = models.CharField(max_length=20, null=True, blank=True)
    genre = models.CharField(max_length=1, choices=GENRE_CHOICES, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    area = models.CharField(max_length=150, null=True, blank=True)
    city = models.CharField(max_length=15, null=True, blank=True)
    state = models.CharField(max_length=15, null=True, blank=True)
    country = models.CharField(max_length=15, null=True, blank=True)
    pincode = models.CharField(max_length=6, null=True, blank=True)
    phone = models.CharField(max_length=10,null=True, blank=True)
    email = models.CharField(max_length=150, null=True, blank=True)
    marital_status = models.CharField(max_length=1, choices=MARITAL_STATUS_CHOICES, null=True, blank=True)
    education = models.CharField(max_length=150, null=True, blank=True)
    education_detail = models.TextField(null=True, blank=True)
    occupation = models.CharField(max_length=150, null=True, blank=True)
    occupation_detail = models.TextField(null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False, auto_created=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True, auto_created=True)

    objects = SubprofileManager()

    def __str__(self):
        return str(self.first_name)

    def get_absolute_url(self):
        return self.user.profile.get_absolute_url()

    def get_update_url(self):
        return reverse("profile:subprofile_update", kwargs={"id": self.id})

    def get_delete_url(self):
        return reverse("profile:subprofile_delete", kwargs={"id": self.id})

    class Meta:
        ordering = ["id"]


class blog(models.Model):
    blog_title = models.CharField(max_length=255, null=True, blank=True)
    blog_text = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.blog_title)

    class Meta:
        ordering = ["-id"]


class ImageBlog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    imageblog_title = models.CharField(max_length=255, null=True, blank=True)
    mainimage = models.FileField(null=True, blank=True)
    status = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.imageblog_title)

    def get_absolute_url(self):
        return reverse("profile:imageblog_detail", kwargs={"id": self.id})

    def get_update_url(self):
        return reverse("profile:imageblog_update", kwargs={"id": self.id})

    def get_create_url(self):
        return reverse("profile:imageblog_detailcreate", kwargs={"id": self.id})


    class Meta:
        ordering = ["-id"]


class SubImageBlogManager(models.Manager):
    def filter_by_instance(self, instance):
        content_type = ContentType.objects.get_for_model(ImageBlog)
        obj_id = instance.id
        qs = super(SubImageBlogManager, self).filter(content_type=content_type, object_id=obj_id)
        return qs


class SubImageBlog(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    extraimage = models.FileField(null=True, blank=True)
    status = models.IntegerField(null=True, blank=True)

    objects = SubImageBlogManager()

    class Meta:
        ordering = ["-id"]
