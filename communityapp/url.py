from django.urls import path
from django.views.generic import TemplateView

from .views import User_Create, User_Login, profile_create, profile_delete, profile_detail, directory_list, \
    profile_update, User_logout, blog_view, imageblog_create, imageblog_delete, imageblog_detail, imageblog_update, \
    imageblog_list, imageblog_detail_with_create, subprofile_detail, subprofile_update, \
    subprofile_delete

app_name = "profile"

urlpatterns = [
    path('', directory_list, name="profile_list"),

    path('login', User_Login.as_view(), name="login"),
    path('signup', User_Create.as_view(), name="signup"),
    path('logout', User_logout, name="logout"),

    path('profile/<int:id>/detail', profile_detail, name="profile_detail"),
    path('profile/create', profile_create, name="profile_create"),
    path('profile/<int:id>/update', profile_update, name="profile_update"),
    path('profile/<int:id>/delete', profile_delete, name="profile_delete"),

    path('subprofile/<int:id>/detail', subprofile_detail, name="subprofile_detail"),
    path('subprofile/<int:id>/update', subprofile_update, name="subprofile_update"),
    path('subprofile/<int:id>/delete', subprofile_delete, name="subprofile_delete"),

    path('imageblog', imageblog_list, name="imageblog_list"),
    path('imageblog/<int:id>/detail', imageblog_detail, name="imageblog_detail"),
    path('imageblog/<int:id>/detailcreate', imageblog_detail_with_create, name="imageblog_detailcreate"),
    path('imageblog/create', imageblog_create, name="imageblog_create"),
    path('imageblog/<int:id>/update', imageblog_update, name="imageblog_update"),
    path('imageblog/<int:id>/delete', imageblog_delete, name="imageblog_delete"),

    path('gallery',TemplateView.as_view(template_name='templates/photo.html'), name='gallery'),

    path('news',TemplateView.as_view(template_name='templates/news.html'), name='news'),
    path('blog', blog_view, name="blog-view"),
]
