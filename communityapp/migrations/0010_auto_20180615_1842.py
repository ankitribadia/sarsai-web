# Generated by Django 2.0.6 on 2018-06-15 13:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('communityapp', '0009_subprofile_relation'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subimageblog',
            old_name='mainimage',
            new_name='extraimage',
        ),
        migrations.AddField(
            model_name='imageblog',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='profile',
            name='address',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='subprofile',
            name='address',
            field=models.TextField(blank=True, null=True),
        ),
    ]
