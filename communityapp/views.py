from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.messages import error
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.contrib import auth
from django.core.paginator import Paginator
from django.urls import reverse, reverse_lazy
from django.views.generic.base import View

from .way2sms import sms as way2sms
from .models import profile, blog, SubProfile, ImageBlog, SubImageBlog
from .form import UserProfileCreateForm, UserLoginForm, UserCreateForm, SubProfileCreateForm, ImageBlogCreateForm, \
    SubImageBlogCreateForm
from itertools import chain


def get_imageblog_create_url():
    return reverse("profile:imageblog_create")


@login_required
def directory_list(request):
    profile_list = profile.objects.ExcludeAdminUser()
    sub_profile_list = SubProfile.objects.all()
    queryset_list = sorted(
        chain(profile_list, sub_profile_list),
        key=lambda user: user.id, reverse=True)

    context = {
        "title": "Directory",
        "Profile_List": queryset_list
    }
    return render(request, "templates/list.html", context)


@login_required
def profile_detail(request, id=None):
    instance = get_object_or_404(profile, id=id)
    if instance.user == request.user or request.user.is_superuser:
        subprofiles = SubProfile.objects.filter_by_instance(instance)
        subprofile_form = SubProfileCreateForm(request.POST or None)
        if subprofile_form.is_valid():
            form_instance = subprofile_form.save(commit=False)
            form_instance.content_type = ContentType.objects.get(model="profile")
            form_instance.object_id = id
            form_instance.user = request.user
            if not form_instance.address:
                form_instance.address = instance.address
            if not form_instance.area:
                form_instance.area = instance.area
            if not form_instance.city:
                form_instance.city = instance.city
            if not form_instance.state:
                form_instance.state = instance.state
            if not form_instance.country:
                form_instance.country = instance.country
            if not form_instance.pincode:
                form_instance.pincode = instance.pincode
            if form_instance.phone == (None or 0):
                form_instance.phone = instance.phone
            if form_instance.email == None:
                form_instance.email = instance.email
            form_instance.save()
            return HttpResponseRedirect(instance.get_absolute_url())
        context = {
            "title": "Detail",
            "item": instance,
            "subprofiles": subprofiles,
            "subprofile_form": subprofile_form
        }
        return render(request, "templates/profile/detail.html", context)
    raise Http404


@login_required
def profile_create(request):
    form = UserProfileCreateForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        "title": "Create",
        "form": form
    }
    return render(request, "templates/profile/create.html", context)


@login_required
def profile_update(request, id=None):
    instance = get_object_or_404(profile, id=id)
    # if instance.user == request.user or request.user.is_superuser:
    if request.method == "GET":
        form = UserProfileCreateForm(instance=instance)
        context = {
            "title": "Update",
            "subprofile_form": instance,
            "form": form
        }
        return render(request, "templates/profile/update.html", context)

    if request.method == "POST":
        form = UserProfileCreateForm(request.POST, instance=instance)
        if form.is_valid():
            profiles = form.save(commit=False)
            phonenumber = request.POST.get("phone")
            profiles.user = instance.user
            profiles.save()
            return HttpResponseRedirect(instance.get_absolute_url())

    raise Http404


@login_required
def profile_delete(request, id=None):
    instance = get_object_or_404(profile, id=id)
    if instance.user == request.user or request.user.is_superuser:
        instance.delete()
        return redirect("profile:profile_list")
    raise Http404


################################################################SUBPROFILE#################################################################

@login_required
def subprofile_detail(request, id=None):
    instance = get_object_or_404(SubProfile, id=id)
    if instance.user == request.user or request.user.is_superuser:
        context = {
            "title": "Detail",
            "item": instance,
        }
        return render(request, "templates/subprofile/detail.html", context)
    raise Http404


@login_required
def subprofile_update(request, id=None):
    instance = get_object_or_404(SubProfile, id=id)
    # if instance.user == request.user or request.user.is_superuser:
    if request.method == "GET":
        form = UserProfileCreateForm(instance=instance)
        context = {
            "title": "Update",
            "subprofile_form": instance,
            "form": form
        }
        return render(request, "templates/subprofile/update.html", context)

    if request.method == "POST":
        form = UserProfileCreateForm(request.POST, instance=instance)
        if form.is_valid():
            profiles = form.save(commit=False)
            phonenumber = request.POST.get("phone")
            profiles.user = instance.user
            profiles.save()
            return HttpResponseRedirect(instance.get_absolute_url())

    raise Http404


@login_required
def subprofile_delete(request, id=None):
    instance = get_object_or_404(SubProfile, id=id)
    if instance.user == request.user or request.user.is_superuser:
        instance.delete()
        return HttpResponseRedirect(instance.get_absolute_url())
    raise Http404


#####################################################################USER#############################################
class User_Login(View):
    form_class = UserLoginForm
    template_name = 'templates/login/login.html'

    def get(self, request):
        form = self.form_class(None)
        if error:
            context = {
                'form': form,
                'error': error,
                'title': 'Login'
            }
            return render(request, self.template_name, context)
        else:
            context = {
                'form': form,
                'title': 'Login'
            }
            return render(request, self.template_name, context)

    def post(self, request):
        number = request.POST.get('number', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=number, password=password)

        if user is not None and user.is_active:
            # Correct password, and the user is marked "active"
            auth.login(request, user)
            # Redirect to a success page.
            instance = profile.objects.get(user=user)
            return HttpResponseRedirect(instance.get_absolute_url())
        else:
            # Show an error page
            error = True
            return HttpResponseRedirect("/login", {'error': error})


class User_Create(View):
    form_class = UserCreateForm
    template_name = 'templates/login/signup.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():

            user = form.save(commit=False)

            # cleaned (normalized) data
            number = form.cleaned_data['number']
            password = form.cleaned_data['password']
            password2 = form.cleaned_data['password2']
            if User.objects.filter(username=number).exists():

                return render(request, self.template_name, {'form': form, 'error': "User Already Exists."})
            user.username = number
            if password == password2:
                passw = password
                user.set_password(passw)
            user.save()

            # return user objects if credentials are correct
            user = authenticate(username=number, password=password)

            if user is not None:

                if user.is_active:
                    login(request, user)
                    iduser = profile.objects.filter(phone=number)[0:1].get()
                    return HttpResponseRedirect(iduser.get_update_url())
        return render(request, self.template_name, {'form': form})


def User_logout(request):
    logout(request)
    return redirect("profile:login")


#################################################################BLOG##################################################################
@login_required
def blog_view(request):
    blogs = blog.objects.all()
    context = {
        "title": "Blog",
        "Blogs": blogs,
    }
    return render(request, "templates/blog.html", context)


################################################################IMAGEBLOG##################################################################
@login_required()
def imageblog_list(request):
    imagebloglist = ImageBlog.objects.all()
    paginator = Paginator(imagebloglist, 20)
    page = request.GET.get('page')
    queryset = paginator.get_page(page)
    context = {
        "title": "Directory",
        "Profile_List": queryset
    }
    return render(request, "templates/imageblog/list.html", context)


@login_required
def imageblog_detail(request, id=None):
    instance = get_object_or_404(ImageBlog, id=id)
    subimages = SubImageBlog.objects.filter_by_instance(instance)
    context = {
        "title": "Detail",
        "item": instance,
        "subprofiles": subimages,
    }
    return render(request, "templates/imageblog/detail.html", context)


@login_required
def imageblog_create(request):
    if request.method == "GET":
        form = ImageBlogCreateForm(None)
        context = {
            "title": "Create",
            "form": form
        }
        return render(request, "templates/imageblog/create.html", context)
    if request.method == "POST":
        form = ImageBlogCreateForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            imageblog_title = request.POST.get("imageblog_title")
            mainimage = request.FILES.get("mainimage")
            instance.imageblog_title = imageblog_title
            instance.mainimage = mainimage
            instance.user = request.user
            instance.save()
            return HttpResponseRedirect(instance.get_absolute_url())
        return HttpResponseRedirect(get_imageblog_create_url())


@login_required
def imageblog_update(request, id=None):
    instance = get_object_or_404(ImageBlog, id=id)
    if instance.user == request.user or request.user.is_superuser:
        if request.method == "GET":
            form = ImageBlogCreateForm(instance=instance)
            context = {
                "title": "Create",
                "item": instance,
                "form": form
            }
            return render(request, "templates/imageblog/update.html", context)
        if request.method == "POST":
            form = ImageBlogCreateForm(request.POST, request.FILES, instance=instance)
            if form.is_valid():
                instance = form.save(commit=False)
                imageblog_title = request.POST.get("imageblog_title")
                mainimage = request.FILES.get("mainimage")
                instance.imageblog_title = imageblog_title
                instance.mainimage = mainimage
                instance.user = request.user
                instance.save()
                return HttpResponseRedirect(instance.get_absolute_url())
            return HttpResponseRedirect(get_imageblog_create_url())
    raise Http404


@login_required
def imageblog_delete(request, id=None):
    instance = get_object_or_404(ImageBlog, id=id)
    if instance.user == request.user or request.user.is_superuser:
        instance.delete()
        return redirect("profile:imageblog_list")
    raise Http404


###########################################################GALLERY###########################################################
def imageblog_detail_with_create(request, id=None):
    instance = get_object_or_404(ImageBlog, id=id)
    if instance.user == request.user or request.user.is_superuser:
        subimages = SubImageBlog.objects.filter_by_instance(instance)
        print(subimages)
        subimage_form = SubImageBlogCreateForm(request.POST or None, request.FILES or None)
        if subimage_form.is_valid():
            form_instance = subimage_form.save(commit=False)
            form_instance.content_type = ContentType.objects.get_for_model(ImageBlog)
            form_instance.object_id = id
            form_instance.extraimage = request.POST.get("extraimage")
            form_instance.save()
            return HttpResponseRedirect(instance.get_create_url())
        context = {
            "title": "Blog_Create",
            "item": instance,
            "subprofiles": subimages,
            "subprofile_form": subimage_form
        }
        return render(request, "templates/imageblog/detail_create.html", context)
    raise Http404
